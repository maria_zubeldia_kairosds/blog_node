import mongoose from 'mongoose';
import { logger } from './logger';

const connectToDB = async () => {
	const host = process.env.MONGO_HOST ?? 'localhost';
	logger.error(`esto es el host ${host}`);
	const port = process.env.MONGO_PORT ?? '27018';
	const dbName = process.env.MONGO_DB ?? 'blog';
	try {
		await mongoose.connect(`mongodb://${host}:${port}/${dbName}`, {
			authSource: 'admin',
			auth: {
				username: process.env.MONGO_INITDB_ROOT_USERNAME ?? 'admin',
				password: process.env.MONGO_INITDB_ROOT_PASSWORD ?? 'admin',
			},
		});
	} catch (error) {
		console.log(error);
	}
};

export { connectToDB };
