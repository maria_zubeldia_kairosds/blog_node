import Container from 'typedi';
import { User, UserType } from '../../domain/model/entities/user.entity';
import { IdVO } from '../../domain/model/vos/id.vo';
import { EmailVO } from '../../domain/model/vos/user/email.vo';
import { PasswordVo } from '../../domain/model/vos/user/password.vo';
import { Role, RoleVO } from '../../domain/model/vos/user/rol.vo';
import { UserService } from '../../domain/services/user.service';

const populate = async (): Promise<void> => {
	const userService = Container.get(UserService);
	const userData: UserType = {
		id: IdVO.create(),
		email: EmailVO.create('admin@admin.com'),
		password: PasswordVo.create('password'),
		role: RoleVO.create(Role.ADMIN),
	};
	userService.persist(new User(userData));
};

export { populate as populateDatabases };
