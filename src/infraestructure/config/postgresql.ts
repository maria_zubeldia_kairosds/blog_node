import { Sequelize } from 'sequelize';
import { logger } from './logger';
import { populateDatabases } from './populate';


const host = process.env.POSTGRES_HOST ?? 'localhost';
logger.error(`este es el host${host}`);
const user = process.env.POSTGRES_USER  ?? 'pguser';
const pass = process.env.POSTGRES_PASSWORD ?? 'pguser';
const port = process.env.POSTGRES_PORT ?? '31751';
const dbname = process.env.POSTGRES_DBNAME ?? 'pgdb';

//postgres://pguser:pguser@${host}:5432/pgdb
const sequelize = new Sequelize(
	`postgres://${user}:${pass}@${host}:${port}/${dbname}`
);
//"postgres://pguser:pguser@dbpostgres:5432/pgdb";
//establece conexión
sequelize.authenticate()
	.then(() => console.log('Connection success'))
	.catch((err) => console.log(err));

//sincroniza sistema de db con postgres
sequelize
	.sync({ force: true })
	.then(() => console.log('Database & tables created'))
	.then(() => populateDatabases());
   
export default sequelize;

/* para que no dropee las tablas al iniciar

sequelize
  .sync({ alter: { drop: false } })
  .then(() => {
    console.log("Database & tables sync");
  })
  .catch(() => {
    sequelize.sync({ force: true });
  });

  cambiar tb docker-compose, la ruta del volumen a : tu-volumen:/var/lib/postgresql/data 


*/