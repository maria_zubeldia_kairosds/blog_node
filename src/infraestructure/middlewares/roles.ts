import express from 'express';
import { User } from '../../domain/model/entities/user.entity';
import { Role } from '../../domain/model/vos/user/rol.vo';

//crear un array de roles permitidos

export const hasRole = (roles: Role[]) => {
	return async (
		req: express.Request,
		res: express.Response,
		next: express.NextFunction
	): Promise<express.Response | void> => {
		const user = req.user as User;
		const roleUser = user.role.value;

		const allow = roles.some((role) => roleUser === role);

		if (allow) {
			next();
			return;
		}
		return res.status(403).json({ status: 'not allowed' });
	};
};
