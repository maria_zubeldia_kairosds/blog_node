import { ExtractJwt, Strategy, StrategyOptions } from 'passport-jwt';
import Container from 'typedi';
import { EmailVO } from '../../domain/model/vos/user/email.vo';
import { UserService } from '../../domain/services/user.service';

const opts: StrategyOptions = {
	jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
	secretOrKey: 'secret', //este secreto tiene quue coincidir con el de la firma del token
};

export default new Strategy(opts, async (payload, done) => {
	try {
		const { email } = payload; //email que viene de la web
		const userService = Container.get(UserService);
		const user = await userService.getByEmail(EmailVO.create(email));
		if (user) {
			//validamos que el email que llega está en la bbdd
			return done(null, user);
		}
		return done(null, false, { message: 'User not found' });
	} catch (err) {
		console.log(err);
	}
});
