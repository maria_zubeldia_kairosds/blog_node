import express from 'express';
import passport from 'passport';

import { body, validationResult } from 'express-validator';
import Container from 'typedi';
import {
	SignInRequest,
	SignInUseCase
} from '../../application/usecases/auth/sign-in.usecase';
import { SignUpUseCase } from '../../application/usecases/auth/sign-up.usecase';
import { User } from '../../domain/model/entities/user.entity';
import { ExceptionWithCode } from '../../domain/model/exception-with-code';

const router = express.Router();

router.post(
	'/api/login',
	body('email').notEmpty(),
	body('password').notEmpty(),

	async (req: express.Request, res: express.Response) => {
		//console.log('hi');
		try {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(400).json({ errors: errors.array() });
			}
			const useCase = Container.get(SignInUseCase);
			const { email, password } = req.body;
			//const { role }: any = req.user;

			const request: SignInRequest = {
				email,
				password,
			};

			const token = await useCase.execute(request);

			if (token) {
				res.status(200).json({ token });
			} else {
				res.status(401).json({ error: 'not permitted' });
			}
		} catch (err) {
			return res.status(err.code).json({ error: err.message });
		}
	}
);

router.post(
	'/api/sign-up',
	body('email').notEmpty(),
	body('password').notEmpty(),
	body('role').notEmpty(),

	async (req: express.Request, res: express.Response) => {
		try {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(400).json({ errors: errors.array() });
			}

			const useCase = Container.get(SignUpUseCase);
			const { email, password, role } = req.body;

			await useCase.execute({ email, password, role });

			res.status(201).json({ status: 'Created!' });
		} catch (err) {
			console.log(err);
			if (err instanceof ExceptionWithCode) {
				return res.status(err.code).json({ error: err.message });
			}
		}
	}
);

//tiene que devolver rol
router.get(
	'/api/auth/role/me',
	passport.authenticate('jwt', { session: false }),
	(req, res) => {
		const user = req.user as User;
		res.status(200).json({ role: user.role.value });

		//const roleUser = user.role.value;

		/*try {
			const user: User | any = req.user;
			if (user) res.status(200).json({role: user.role.value});
		} catch (err){
			console.log(err);
			return res.status(err.code).json({ error: err.message });

			
		}*/
	}
);

router.get('/api/health', (req, res) => {
	//console.log('estoy en api health');

	return res.status(200).json({ mensaje: 'healthy' });
});

export { router as authRouter };
