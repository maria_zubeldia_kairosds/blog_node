import express from 'express';
import { body, validationResult } from 'express-validator';
import passport from 'passport';
import 'reflect-metadata';
import Container from 'typedi';
import { CommentRequest } from '../../application/usecases/comment/comment.request';
import { AddCommentUseCase } from '../../application/usecases/comment/create-comment.usecase';
import { DeleteCommentUseCase } from '../../application/usecases/comment/delete-comment.usecase';
import { GetCommetByIdUseCase } from '../../application/usecases/comment/get-comment-by-id.usecase';
import { IdRequest } from '../../application/usecases/id.request';
import { CreatePostUseCase } from '../../application/usecases/post/create-post.usecase';
import { DeletePostUseCase } from '../../application/usecases/post/delete-post.usecase';
import { GetAllPostsUseCase } from '../../application/usecases/post/get-all-post.usecase';
import { GetPostByIdUseCase } from '../../application/usecases/post/get-post-by-id.usecase';
import { PostRequest } from '../../application/usecases/post/post.request';
import { PostResponse } from '../../application/usecases/post/post.response';
import { UpdatePostUseCase } from '../../application/usecases/post/update-post.usecase';
import { User } from '../../domain/model/entities/user.entity';
import { Role } from '../../domain/model/vos/user/rol.vo';
import { cache, updateCache } from '../cache/middleware';
import { hasRole } from '../middlewares/roles';

const router = express.Router();

router.post(
	'/api/posts',
	body('title').notEmpty(),
	body('content').notEmpty(),
	passport.authenticate('jwt', { session: false }),
	hasRole([Role.ADMIN, Role.AUTHOR]),

	async (req: express.Request, res: express.Response) => {
		try {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(400).json({ errors: errors.array() });
			}
			const useCase = Container.get(CreatePostUseCase);
			const user = req.user as User;
			const request: PostRequest = {
				title: req.body.title,
				userEmail: req.body.userEmail,
				content: req.body.content,
			};

			const response = await useCase.execute(request, user.id.value);
			return res.status(201).json({ response });
		} catch (err: any) {
			return res.status(500).json({ error: err.message });
		}
	}
);

//get all posts logged
router.get(
	'/api/posts',
	cache(),
	passport.authenticate('jwt', { session: false }),
	hasRole([Role.ADMIN, Role.AUTHOR, Role.USER]),

	async (req: express.Request, res: express.Response) => {
		try {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(400).json({ errors: errors.array() });
			}
			const useCase = Container.get(GetAllPostsUseCase);

			const posts: PostResponse[] = await useCase.execute();
			console.log('<<<<---------from pg----------->>>>>');

			res.locals.data = posts;
			return res.status(200).json(posts);
		} catch (err: any) {
			return res.status(500).json({ error: err.message });
		}
	}
);

//get one post by id logged
router.get(
	'/api/posts/:postId',
	cache(),
	passport.authenticate('jwt', { session: false }),
	hasRole([Role.ADMIN, Role.AUTHOR, Role.USER]),

	async (req: express.Request, res: express.Response) => {
		try {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(400).json({ errors: errors.array() });
			}
			const useCase = Container.get(GetPostByIdUseCase);
			const postId: IdRequest = req.params.postId;

			const post = await useCase.execute(postId);
			res.locals.data = post;
			return res.status(200).json(post);
		} catch (err: any) {
			return res.status(500).json({ error: err.message });
		}
	}
);

//actualizar post
router.put(
	'/api/posts/',
	passport.authenticate('jwt', { session: false }),
	hasRole([Role.ADMIN, Role.AUTHOR]),
	body('id').notEmpty(),
	body('title').notEmpty(),
	body('content').notEmpty(),
	updateCache(),

	async (req: express.Request, res: express.Response) => {
		try {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(400).json({ errors: errors.array() });
			}
			const useCase = Container.get(UpdatePostUseCase);
			const id: IdRequest = req.body.id;

			const { title, content, userEmail } = req.body;

			const post = await useCase.execute(id, { title, content, userEmail });

			return res.status(200).json(post);
		} catch (err: any) {
			return res.status(500).json({ error: err.message });
		}
	}
);

//borrar un post by id
router.delete(
	'/api/posts/:postId',
	passport.authenticate('jwt', { session: false }),
	hasRole([Role.ADMIN, Role.AUTHOR]),
	updateCache(),
	async (req: express.Request, res: express.Response) => {
		try {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(400).json({ errors: errors.array() });
			}

			const postId: IdRequest = req.params.postId;

			const useCase = Container.get(DeletePostUseCase);
			await useCase.execute(postId);
			return res.status(200).json({ postId });
		} catch (err: any) {
			return res.status(500).json({ error: err.message });
		}
	}
);

/******************************** comentarios  *********************************** */
//crear un comentario en un post específico
router.post(
	'/api/posts/:idPost/comments',
	body('email').notEmpty(),
	body('content').notEmpty(),
	passport.authenticate('jwt', { session: false }),
	hasRole([Role.ADMIN, Role.AUTHOR, Role.USER]),

	async (req: express.Request, res: express.Response) => {
		try {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(400).json({ errors: errors.array() });
			}

			const idPost = req.params.idPost;
			const user = req.user as User;

			const useCase = Container.get(AddCommentUseCase);

			const request: CommentRequest = {
				postId: idPost,
				userEmail: req.body.email,
				content: req.body.content,
			};
			//console.log('esta es la req ', request);

			const response = await useCase.execute(request, user.id.value);
			return res.status(201).json(response);
		} catch (err: any) {
			return res.status(500).json({ error: err.message });
		}
	}
);

router.delete(
	'/api/posts/comments/:idComment',
	passport.authenticate('jwt', { session: false }),
	hasRole([Role.ADMIN, Role.AUTHOR]),

	async (req: express.Request, res: express.Response) => {
		try {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(400).json({ errors: errors.array() });
			}

			//const idPost = req.params.idPost;
			const idComment = req.params.idComment;

			const useCase = Container.get(DeleteCommentUseCase);

			const request = { idComment };

			await useCase.execute(request);

			return res.status(204).json();
		} catch (err: any) {
			return res.status(500).json({ error: err.message });
		}
	}
);

//get comment by id logged
router.get(
	'/api/posts/comments/:commentId',
	passport.authenticate('jwt', { session: false }),
	hasRole([Role.ADMIN]),

	async (req: express.Request, res: express.Response) => {
		try {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(400).json({ errors: errors.array() });
			}
			const useCase = Container.get(GetCommetByIdUseCase);
			const commentId: IdRequest = req.params.commentId;
			console.log('comment id router ', commentId);
			
			const post = await useCase.execute(commentId);

			return res.status(200).json(post);
		} catch (err: any) {
			return res.status(500).json({ error: err.message });
		}
	}
);


/************************  PUBLIC PATHS *********************************** */ 


//get all posts public
router.get(
	'/api/posts-public',
	cache(),

	async (req: express.Request, res: express.Response) => {
		try {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(400).json({ errors: errors.array() });
			}
			const useCase = Container.get(GetAllPostsUseCase);

			const posts: PostResponse[] = await useCase.execute();
			console.log('<<<<---------from pg----------->>>>>');

			res.locals.data = posts;
			return res.status(200).json(posts);
		} catch (err: any) {
			return res.status(500).json({ error: err.message });
		}
	}
);

//get one post by id public
router.get(
	'/api/posts-public/:postId',
	cache(),

	async (req: express.Request, res: express.Response) => {
		try {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(400).json({ errors: errors.array() });
			}
			const useCase = Container.get(GetPostByIdUseCase);
			const postId: IdRequest = req.params.postId;

			const post = await useCase.execute(postId);
			res.locals.data = post;
			return res.status(200).json(post);
		} catch (err: any) {
			return res.status(500).json({ error: err.message });
		}
	}
);

//get comment by id public
router.get(
	'/api/posts-public/comments/:commentId',

	async (req: express.Request, res: express.Response) => {
		try {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(400).json({ errors: errors.array() });
			}
			const useCase = Container.get(GetCommetByIdUseCase);
			const commentId: IdRequest = req.params.commentId;
			console.log('comment id router ', commentId);
			
			const post = await useCase.execute(commentId);

			return res.status(200).json(post);
		} catch (err: any) {
			return res.status(500).json({ error: err.message });
		}
	}
);

export { router as postsRouter };
