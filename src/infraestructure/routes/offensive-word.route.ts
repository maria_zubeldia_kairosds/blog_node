import express from 'express';
import { body, validationResult } from 'express-validator';
import passport from 'passport';
import Container from 'typedi';
import { CreateOffensiveWordUseCase } from '../../application/usecases/create-offensive-word.usecase';
import { DeleteOffensiveWordUseCase } from '../../application/usecases/delete-offensive-word.usecase';
import { GetAllOffensiveWordsUseCase } from '../../application/usecases/get-all-offensive-words.usecase';
import { OffensiveWordRequest } from '../../application/usecases/offensive-word.request';
import { OffensiveWordResponse } from '../../application/usecases/offensive-word.response';
import { UpdateOffensiveWordUseCase } from '../../application/usecases/update-offensive-word.usecase';
import { Role } from '../../domain/model/vos/user/rol.vo';
import { hasRole } from '../middlewares/roles';

const router = express.Router();

router.post(
	'/api/offensive-word',
	body('word').notEmpty().escape(),
	body('level').notEmpty().isNumeric(),
	
	async (req, res) => {
		const { word, level } = req.body;
		const offensiveWordRequest: OffensiveWordRequest = {
			word,
			level,
		};

		const errors: any = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}
		const useCase = Container.get(CreateOffensiveWordUseCase);

		const offensiveWordResponse = await useCase.execute(offensiveWordRequest);
		return res.status(201).send(offensiveWordResponse);
	}
);

router.get(
	'/api/offensive-word',
	passport.authenticate('jwt', { session: false }),
	hasRole([Role.ADMIN]),

	async (req, res) => {
		const useCase = Container.get(GetAllOffensiveWordsUseCase);

		const offensiveWords: OffensiveWordResponse[] = await useCase.execute();
		return res.status(200).json(offensiveWords);
	}
);

router.delete('/api/offensive-word/:id', async (req, res) => {
	try {
		const idDeleted = req.params.id;

		const useCase = Container.get(DeleteOffensiveWordUseCase);

		await useCase.execute(idDeleted);

		return res.status(201).json('Deleted!');
	} catch (error) {
		console.log(error);
	}


});

router.put('/api/offensive-word/:id', async (req, res) => {
	try {
		const idUpdated = req.params.id;
		const { word, level } = req.body;
		const offensiveWordRequest: OffensiveWordRequest = { word, level };

		const useCase = Container.get(UpdateOffensiveWordUseCase);

		await useCase.execute(idUpdated, offensiveWordRequest);
		return res.status(201).json({
			id: idUpdated,
			word: offensiveWordRequest.word,
			level: offensiveWordRequest.level,
		});
		//return res.status(201).send('Updated!');
	} catch (error) {
		console.log(error);
	}


});

export { router as offensiveWordRouter };
