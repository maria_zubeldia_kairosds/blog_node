import {
	Comment,
	CommentType
} from '../../domain/model/entities/comment.entity';
import { Post, PostType } from '../../domain/model/entities/post.entity';
import { ContentCommentVO } from '../../domain/model/vos/content-comment.vo';
import { IdVO } from '../../domain/model/vos/id.vo';
import { ContentVO } from '../../domain/model/vos/post/content.vo';
import { TitleVO } from '../../domain/model/vos/post/title.vo';
import { TimestampVO } from '../../domain/model/vos/timestamp.vo';
import { EmailVO } from '../../domain/model/vos/user/email.vo';
import { PostRepository } from '../../domain/repositories/post.repository';
import { CommentModel } from './comment.schema';
import { PostModel } from './post.schema';
import { UserModel } from './users.schema';

export class PostRepositoryPG implements PostRepository {
	async save(post: Post, userId: string): Promise<void> {
		const id = post.id.value;
		const title = post.title.value;
		const content = post.content.value;

		const postModel = PostModel.build({ id, title, content, userId });
		await postModel.save();
	}

	async getById(id: IdVO): Promise<Post | null> {
		const postDB: any = await PostModel.findOne({
			where: { id: id.value },
			include: [UserModel, { model: CommentModel, include: [UserModel] }],
		});
		if (!postDB) {
			return null;
		}

		const comments: Comment[] = postDB.comments.map((cDB: any) => {
			const commentData: CommentType = {
				content: ContentCommentVO.create(cDB.content),
				id: IdVO.createWithUUID(cDB.id),
				emailAuthor: EmailVO.create(cDB.user.email),
				timestamp: TimestampVO.create(),
			};
			return new Comment(commentData);
		});

		const postData: PostType = {
			id: IdVO.createWithUUID(postDB.id),
			authorEmail: EmailVO.create(postDB.authorEmail),
			title: TitleVO.create(postDB.title),
			content: ContentVO.create(postDB.content),
			comments,
		};
		return new Post(postData);
	}

	async getAll(): Promise<Post[]> {
		const postList = await PostModel.findAll({
			include: [ { model: CommentModel }],
			//include: [UserModel, { model: CommentModel, include: [UserModel] }],
		});

		return postList.map((postModel: any) => {
			const postData: PostType = {
				id: IdVO.createWithUUID(postModel.id),
				authorEmail: EmailVO.create(postModel.authorEmail),
				title: TitleVO.create(postModel.title),
				content: ContentVO.create(postModel.content),

				comments: postModel.comments.map((c: any) => {
					const commentData: CommentType = {
						content: ContentCommentVO.create(c.content),
						id: IdVO.createWithUUID(c.id),
						emailAuthor: EmailVO.create(c.email),
						timestamp: TimestampVO.create(),
					};
					return new Comment(commentData);
				}),
			};
			console.log(postData);

			return new Post(postData);
		});
	}

	async delete(id: IdVO): Promise<void> {
		await PostModel.destroy({
			where: { id: id.value },
		});
	}

	async update(post: Post): Promise<void> {
		const postDB = {
			id: post.id.value,
			title: post.title.value,
			content: post.content.value,
		};
		await PostModel.update(postDB, { where: { id: postDB.id } });
	}

	async addComment(
		post: Post,
		comment: Comment,
		userId: string
	): Promise<void> {
		const id = comment.id.value;
		const content = comment.content.value;
		const userEmail = comment.emailAuthor.value;
		const postId = post.id.value;

		const newComment = CommentModel.build({
			id,
			content,
			userEmail,
			userId,
			postId,
		});
		console.log('repository add comment ', newComment);

		await newComment.save();
	}

	async deleteComment(commentIdVO: IdVO): Promise<void> {

		await CommentModel.destroy({ where: { id: commentIdVO.value } });
	}

	async getCommentById(commentId: IdVO): Promise<Comment | null> {
		const commentDb: any = await CommentModel.findOne({
			where: { id: commentId.value },

		});
		console.log('db repo ', commentDb);
		
		if (!commentDb) return null;
		
	
		const newComment: CommentType = {
			id: IdVO.createWithUUID(commentDb.id),
			emailAuthor: EmailVO.create(commentDb.emailAuthor),
			content: ContentCommentVO.create(commentDb.content),
			timestamp: TimestampVO.create(),
		};

		return new Comment(newComment);
	}
}
