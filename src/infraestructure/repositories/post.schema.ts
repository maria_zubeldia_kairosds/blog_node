import { DataTypes } from 'sequelize';
import sequelize from '../config/postgresql';

const PostModel = sequelize.define('post', {
	/* 
  id: IdVO;
  authorEmail: EmailVO;
  title: TitleVO;
  content: ContentVO;
  comments: string[];
    */

	id: {
		type: DataTypes.UUID,
		allowNull: false,
		primaryKey: true,
	},

	title: {
		type: DataTypes.STRING,
		allowNull: false,
		unique: true,
	},

	content: {
		type: DataTypes.STRING,
		allowNull: false,
    
	},

	//comments: {
	//	type: DataTypes.ARRAY(String),
        
	//}
});
export { PostModel };
