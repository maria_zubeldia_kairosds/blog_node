import { DataTypes } from 'sequelize';
import sequelize from '../config/postgresql';

const CommentModel = sequelize.define('comment', {
	id: {
		type: DataTypes.UUID,
		allowNull: false,
		primaryKey: true,
	},

	content: {
		type: DataTypes.STRING,
		allowNull: false,
	},
});

export { CommentModel };
