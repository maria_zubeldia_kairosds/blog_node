import { User, UserType } from '../../domain/model/entities/user.entity';
import { IdVO } from '../../domain/model/vos/id.vo';
import { EmailVO } from '../../domain/model/vos/user/email.vo';
import { PasswordVo } from '../../domain/model/vos/user/password.vo';
import { RoleVO } from '../../domain/model/vos/user/rol.vo';
import { UserRepository } from '../../domain/repositories/user.repository';
import { UserModel } from './users.schema';

export class UserRepositoryPG implements UserRepository {
	async save(user: User): Promise<void> {
		const id = user.id.value;
		const email = user.email.value;
		const password = user.password.value;
		const role = user.role.value;

		const userModel = UserModel.build({ id, email, password, role });
		await userModel.save();
	}

	async getByEmail(email: EmailVO): Promise<User | null> {
		const user: any = await UserModel.findOne({
			where: { email: email.value },
		});
		if (!user) {
			return null;
		}

		const userData: UserType = {
			id: IdVO.createWithUUID(user.id),
			email: EmailVO.create(user.email),
			password: PasswordVo.create(user.password),
			role: RoleVO.create(user.role)
		};

		return new User(userData);
	}
}
