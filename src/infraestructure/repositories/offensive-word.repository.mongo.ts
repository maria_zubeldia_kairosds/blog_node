import { AnyObject } from 'mongoose';
import {
	OffensiveWord,
	OffensiveWordType
} from '../../domain/model/entities/offensive-word.entity';
import { IdVO } from '../../domain/model/vos/id.vo';
import { LevelVO } from '../../domain/model/vos/level.vo';
import { WordVO } from '../../domain/model/vos/word.vo';
import { OffensiveWordRepository } from '../../domain/repositories/offensive-word.repository';
import { OffensiveWordModel } from './offensive-word.schema';

export class OffensiveWordRepositoryMongo implements OffensiveWordRepository {
	async save(offensiveWord: OffensiveWord): Promise<void> {
		const newOffensiveWord = {
			id: offensiveWord.id.value,
			word: offensiveWord.word.value,
			level: offensiveWord.level.value,
		};
		const offensiveWordModel = new OffensiveWordModel(newOffensiveWord);
		await offensiveWordModel.save();
	}

	async getAll(): Promise<OffensiveWord[]> {
		const offensiveWordList = await OffensiveWordModel.find({}).exec();
		return offensiveWordList.map((offensiveWordModel: AnyObject) => {
			const offensiveWordData: OffensiveWordType = {
				id: IdVO.createWithUUID(offensiveWordModel.id),
				word: WordVO.create(offensiveWordModel.word),
				level: LevelVO.create(offensiveWordModel.level),
			};
			return new OffensiveWord(offensiveWordData);
		});
	}

	async getById(id: IdVO): Promise<OffensiveWord | null> {
		const offensiveWordDB = await OffensiveWordModel.findOne({
			id: id.value,
		}).exec();
		if (!offensiveWordDB) {
			return null;
		}
		return this.createOffensiveWord(offensiveWordDB);
	}

	async delete(id: IdVO): Promise<void> {
		await OffensiveWordModel.findOneAndRemove({ id: id.value });
	}

	async update(offensiveWord: OffensiveWord): Promise<void> {
		await OffensiveWordModel.findOneAndUpdate(
			{ id: offensiveWord.id.value },
			{ word: offensiveWord.word.value, level: offensiveWord.level.value },
			//{ new: true }
		);
	}

	private createOffensiveWord(offensiveWordDB: AnyObject) {
		const offensiveWordData: OffensiveWordType = {
			id: IdVO.createWithUUID(offensiveWordDB.id),
			word: WordVO.create(offensiveWordDB.word),
			level: LevelVO.create(offensiveWordDB.level),
		};
		return new OffensiveWord(offensiveWordData);
	}
}
