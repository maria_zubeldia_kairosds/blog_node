import express from 'express';
import Container from 'typedi';
import { GetAllPostsUseCase } from '../../application/usecases/post/get-all-post.usecase';

import { Cache, CacheRepositoryMongo } from './cache.repository.mongo';

const TIME_RESET = 70;

const saveData = async ({ key, data }: Cache) => {
	
	const cacheRepository = Container.get(CacheRepositoryMongo);
	await cacheRepository.save({ key, data });

	console.log('-------------------savedata chace ------------------------');

	setTimeout(async () => {
		await cacheRepository.delete(key);
	}, TIME_RESET * 1000);
};

export const cache = () => {
	return async (
		req: express.Request,
		res: express.Response,
		next: express.NextFunction
	): Promise<express.Response | void> => {
		
		const cacheRepository = Container.get(CacheRepositoryMongo);
		//la url de la petición
		const key = req.originalUrl;
		//datos en mongo, los busca según la url
		const data: string | null = await cacheRepository.getByKey(key);
		console.log('in cache-----------------------------------------');
		//si la data existe en mongo, envía la respuesta desde mongo
		if (data) {
			return res.status(200).json(JSON.parse(data));
		}

		//cuando la respuesta termina, comprueba si existe la data en res.locals.data
		//si existe, la guarda
		res.on('finish', async () => {
			const { data } = res.locals;

			if (data) {
				saveData({ key, data: JSON.stringify(data) });
			}
		});

		next();
	};
};

export const updateCache = () => {
	return async (
		req: express.Request,
		res: express.Response,
		next: express.NextFunction
	): Promise<express.Response | void> => {
		console.log('----------------updating cache-------------------------');

		res.on('finish', async () => {
			const cacheRepository = Container.get(CacheRepositoryMongo);
			await cacheRepository.deleteAll();

			const getAllPostsUseCase = Container.get(GetAllPostsUseCase);
			const posts = await getAllPostsUseCase.execute();

			await saveData({ key: '/api/post/', data: JSON.stringify(posts) });
		});

		next();
	};
};
