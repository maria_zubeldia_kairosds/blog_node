jest.mock(
	'./../../infraestructure/repositories/offensive-word.repository.mongo'
);
import 'reflect-metadata';
import Container from 'typedi';
import { OffensiveWordRepositoryMongo } from './../../infraestructure/repositories/offensive-word.repository.mongo';
import { CreateOffensiveWordUseCase } from './create-offensive-word.usecase';
import { OffensiveWordRequest } from './offensive-word.request';

describe('create offensive word use case', () => {
	it('should create offensive word and persist', () => {
		const repository = new OffensiveWordRepositoryMongo();
		Container.set('OffensiveWordRepository', repository);

		const useCase = Container.get(CreateOffensiveWordUseCase);
		const offensiveWordRequest: OffensiveWordRequest = {
			word: 'testeando',
			level: 1,
		};
		useCase.execute(offensiveWordRequest);
		expect(repository.save).toHaveBeenCalled();
	});
});
