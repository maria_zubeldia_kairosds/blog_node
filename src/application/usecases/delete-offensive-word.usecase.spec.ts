/*jest.mock(
	'./../../infraestructure/repositories/offensive-word.repository.mongo'
);*/
import 'reflect-metadata';

import Container from 'typedi';
import { OffensiveWord } from '../../domain/model/entities/offensive-word.entity';
import { IdVO } from '../../domain/model/vos/id.vo';
import { LevelVO } from '../../domain/model/vos/level.vo';
import { WordVO } from '../../domain/model/vos/word.vo';
import { OffensiveWordRepositoryMongo } from './../../infraestructure/repositories/offensive-word.repository.mongo';
import { DeleteOffensiveWordUseCase } from './delete-offensive-word.usecase';

describe('delete offensive word use case',  () => {
	
	it('should delete offensive word', async () => {
		const repository = new OffensiveWordRepositoryMongo();
		Container.set('OffensiveWordRepository', repository);

		jest.spyOn(repository, 'getById').mockResolvedValue(
			new OffensiveWord({
				id: IdVO.createWithUUID('7e66c023-f814-42e2-8d60-bd582b852af6'),
				word: WordVO.create('Test'),
				level: LevelVO.create(3),
			})
		);
		jest.spyOn(repository, 'delete').mockResolvedValue();

		const useCase: DeleteOffensiveWordUseCase = Container.get(
			DeleteOffensiveWordUseCase
		);
		await useCase.execute('7e66c023-f814-42e2-8d60-bd582b852af6');
		expect(repository.delete).toHaveBeenCalled();
	});
});
