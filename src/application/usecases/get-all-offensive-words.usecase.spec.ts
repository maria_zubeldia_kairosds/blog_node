jest.mock(
	'./../../infraestructure/repositories/offensive-word.repository.mongo',
	() => {
		return {
			OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
				return {
					getAll: jest.fn().mockImplementation(() => [
						new OffensiveWord({
							id: IdVO.create(),
							word: WordVO.create('Test'),
							level: LevelVO.create(3),
						}),
					]),
				};
			}),
		};
	}
);

import 'reflect-metadata';

import Container from 'typedi';
import { OffensiveWord } from '../../domain/model/entities/offensive-word.entity';
import { IdVO } from '../../domain/model/vos/id.vo';
import { LevelVO } from '../../domain/model/vos/level.vo';
import { WordVO } from '../../domain/model/vos/word.vo';
import { OffensiveWordRepositoryMongo } from './../../infraestructure/repositories/offensive-word.repository.mongo';
import { GetAllOffensiveWordsUseCase } from './get-all-offensive-words.usecase';

describe('getAllOffensiveWord use case', () => {
	it('should get all offensive words', async () => {
		const repo = new OffensiveWordRepositoryMongo();

		Container.set('OffensiveWordRepository', repo);

		const useCase: GetAllOffensiveWordsUseCase = Container.get(GetAllOffensiveWordsUseCase);
		const offensiveWords = await useCase.execute();

		expect(repo.getAll).toHaveBeenCalled();
		//expect(offensiveWords[0].id).toEqual('7e66c023-f814-42e2-8d60-bd582b852af6');
		expect(offensiveWords[0].word).toEqual('Test');
		expect(offensiveWords[0].level).toBe(3);

	});
});
