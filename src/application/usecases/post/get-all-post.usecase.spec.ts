jest.mock('../../../infraestructure/repositories/post.repository.pg', () => {
	return {
		PostRepositoryPG: jest.fn().mockImplementation(() => {
			return {
				getAll: jest.fn().mockImplementation(() => {
					return [new Post({
						id: IdVO.create(),
						authorEmail: EmailVO.create('maria_kairos@kairos.com'),
						title: TitleVO.create('este es el titulo del test'),
						content: ContentVO.create('este es el contenido del test askdjajjjjjjjklsdjaksd'),
						comments: []
					})];
				}),
			};
		}),
	};
});
import 'reflect-metadata';

import Container from 'typedi';
import { Post } from '../../../domain/model/entities/post.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { ContentVO } from '../../../domain/model/vos/post/content.vo';
import { TitleVO } from '../../../domain/model/vos/post/title.vo';
import { EmailVO } from '../../../domain/model/vos/user/email.vo';
import { PostRepositoryPG } from '../../../infraestructure/repositories/post.repository.pg';
import { GetAllPostsUseCase } from './get-all-post.usecase';

describe('get all posts use case', () => {

	it('should get all posts', async() => {
        
		const repo = new PostRepositoryPG();
		Container.set('PostRepository', repo);
		const useCase = Container.get(GetAllPostsUseCase);

		const posts = await useCase.execute();

		expect(repo.getAll).toHaveBeenCalled();
		expect(posts).toHaveLength(1);
	});
	
    
});