import { CommentResponse } from '../comment/comment.response';

export type PostResponse = {
  id: string;
  authorEmail: string;
  title: string;
  content: string;
  comments: CommentResponse[];
};