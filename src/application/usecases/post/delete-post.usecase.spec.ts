jest.mock('./../../../infraestructure/repositories/post.repository.pg', () => {
	return {
		PostRepositoryPG: jest.fn().mockImplementation(() => {
			return {
				save: jest.fn().mockImplementation(() => {
					return new Post({
						id: IdVO.createWithUUID('1a0fde8a-e773-40c4-a39e-701cc994da18'),
						authorEmail: EmailVO.create('maria_kairos@kairos.com'),
						title: TitleVO.create('titulo test delete'),
						content: ContentVO.create(
							'contenidoajsflkjaksfjawifj alksjkfjaksjfjlaks ...'
						),
						comments: [],
					});
				}),
				delete: jest.fn(),
			};
		}),
	};
});
import 'reflect-metadata';

import Container from 'typedi';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { ContentVO } from '../../../domain/model/vos/post/content.vo';
import { TitleVO } from '../../../domain/model/vos/post/title.vo';
import { EmailVO } from '../../../domain/model/vos/user/email.vo';
import { PostRepositoryPG } from '../../../infraestructure/repositories/post.repository.pg';
import { Post } from './../../../domain/model/entities/post.entity';
import { DeletePostUseCase } from './delete-post.usecase';

describe('delete post use case', () => {

	it('should delete a psot', async() => {
		const repo = new PostRepositoryPG();
		Container.set('PostRepository', repo);

		const useCase = Container.get(DeletePostUseCase);
		const id = '9f1ad109-0a91-4271-89b0-97c31c1ae608';
        
		await useCase.execute(id);

		expect(repo.delete).toHaveBeenCalled();
	});


});