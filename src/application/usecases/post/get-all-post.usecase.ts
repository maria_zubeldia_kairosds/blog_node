import { Service } from 'typedi';
import { Post } from '../../../domain/model/entities/post.entity';
import { PostService } from '../../../domain/services/post.service';
import { PostResponse } from './post.response';

@Service()
export class GetAllPostsUseCase {
	constructor(private postService: PostService) {}

	async execute(): Promise<PostResponse[]> {
		const posts = await this.postService.getAll();

		const postResponse = posts.map((p: Post) => {
			return {
				id: p.id.value,
				authorEmail: p.authorEmail.value,
				title: p.title.value,
				content: p.content.value,
				comments: p.comments.map((c) => {
					return {
						id: c.id.value,
						emailAuthor: c.emailAuthor.value,
						content: c.content.value,
						timestamp: c.timestamp.value,
					};
				}),
			};
		});

		return postResponse;
	}
}
