import { Service } from 'typedi';
import { Post, PostType } from '../../../domain/model/entities/post.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { ContentVO } from '../../../domain/model/vos/post/content.vo';
import { TitleVO } from '../../../domain/model/vos/post/title.vo';
import { EmailVO } from '../../../domain/model/vos/user/email.vo';
import { PostService } from '../../../domain/services/post.service';
import { PostRequest } from './post.request';
import { PostResponse } from './post.response';

@Service()
export class CreatePostUseCase {
	constructor(private postService: PostService){}
    
	async execute(postRequest: PostRequest, userId: string): Promise<PostResponse>{
		
		const postData: PostType = {
			id: IdVO.create(),
			title: TitleVO.create(postRequest.title),
			content: ContentVO.create(postRequest.content),
			authorEmail: EmailVO.create(postRequest.userEmail),
			comments: [],
		};

		await this.postService.persist(new Post(postData), userId);

		const postResponse: PostResponse = {
			id: postData.id.value,
			title: postData.title.value,
			content: postData.content.value,
			authorEmail: postData.authorEmail.value,
			comments: []
		};
		
		return postResponse;
	}
}