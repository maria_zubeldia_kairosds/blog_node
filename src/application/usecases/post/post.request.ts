export type PostRequest = {
    title: string,
    content: string,
    userEmail: string,
}