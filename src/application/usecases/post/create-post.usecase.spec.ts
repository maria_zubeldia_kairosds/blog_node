jest.mock('../../../infraestructure/repositories/post.repository.pg', () => {
	return {
		PostRepositoryPG: jest.fn().mockImplementation(() => {
			return {
				save: jest.fn().mockImplementation(() => {
					return new Post({
						id: IdVO.create(),
						authorEmail: EmailVO.create('maria_kairosds@kairos.com'),
						title: TitleVO.create('titulo test holiiiiiiiiii'),
						content: ContentVO.create(
							'este es el contenido del post de test asdfffffffffffff'
						),
						comments: [],
					});
				}),
			};
		}),
	};
});

import 'reflect-metadata';
import Container from 'typedi';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { ContentVO } from '../../../domain/model/vos/post/content.vo';
import { TitleVO } from '../../../domain/model/vos/post/title.vo';
import { EmailVO } from '../../../domain/model/vos/user/email.vo';
import { PostRepositoryPG } from '../../../infraestructure/repositories/post.repository.pg';
import { Post } from './../../../domain/model/entities/post.entity';
import { CreatePostUseCase } from './create-post.usecase';
import { PostRequest } from './post.request';

describe('create post use case', () => {
	it('should create a new post', async () => {
		const repo = new PostRepositoryPG();
		Container.set('PostRepository', repo);

		const useCase = Container.get(CreatePostUseCase);

		const id = '9f1ad109-0a91-4271-89b0-97c31c1ae608';

		const req: PostRequest = {
			userEmail: 'maria_kairosds@kairos.com',
			title: 'titulo test holiiiiiiiiii',
			content: 'este es el contenido del post de test asdfffffffffffff',
		};

		const post = await useCase.execute(req, id);

		expect(repo.save).toHaveBeenCalled();
		expect(post.authorEmail).toBe('maria_kairosds@kairos.com');
	});
});
