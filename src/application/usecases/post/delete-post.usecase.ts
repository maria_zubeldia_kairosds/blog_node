import { Service } from 'typedi';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { PostService } from '../../../domain/services/post.service';
import { IdRequest } from '../id.request';

@Service()
export class DeletePostUseCase {
	constructor(private postService: PostService){}

	async execute(idRequest: IdRequest): Promise <void>{
		const id = IdVO.createWithUUID(idRequest);
		await this.postService.delete(id);
	}
}