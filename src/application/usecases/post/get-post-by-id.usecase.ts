import { Service } from 'typedi';
import { ExceptionWithCode } from '../../../domain/model/exception-with-code';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { PostService } from '../../../domain/services/post.service';
import { IdRequest } from '../id.request';
import { PostResponse } from './post.response';

@Service()
export class GetPostByIdUseCase {
	constructor(private postService: PostService) {}

	async execute(postId: IdRequest): Promise<PostResponse> {
		const postIdVO = IdVO.createWithUUID(postId);
		const post = await this.postService.getById(postIdVO);
		if (!post) {
			throw new ExceptionWithCode(400, 'post not found');
		}
		const postResponse: PostResponse = {
			id: post.id.value,
			authorEmail: post.authorEmail.value,
			title: post.title.value,
			content: post.content.value,
			comments: post.comments.map((c) => {
				return {
					id: c.id.value,
					content: c.content.value,
					emailAuthor: c.emailAuthor.value,
					timestamp: c.timestamp.value,
				};
			}),
		};
		return postResponse;
	}
}
