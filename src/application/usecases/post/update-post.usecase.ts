import { Service } from 'typedi';
import { Post, PostType } from '../../../domain/model/entities/post.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { ContentVO } from '../../../domain/model/vos/post/content.vo';
import { TitleVO } from '../../../domain/model/vos/post/title.vo';
import { EmailVO } from '../../../domain/model/vos/user/email.vo';
import { PostService } from '../../../domain/services/post.service';
import { IdRequest } from '../id.request';
import { PostRequest } from './post.request';
import { PostResponse } from './post.response';

@Service()
export class UpdatePostUseCase {
	constructor(private postService: PostService) {}

	async execute(
		id: IdRequest,
		postRequest: PostRequest
	): Promise<PostResponse> {
		const postData: PostType = {
			id: IdVO.createWithUUID(id),
			authorEmail: EmailVO.create(postRequest.userEmail),
			title: TitleVO.create(postRequest.title),
			content: ContentVO.create(postRequest.content),
			comments: [],
		};

		const updatedPost = new Post(postData);
		await this.postService.update(updatedPost);

		//devolver objeto en lugar de void
		const postResponse: PostResponse = {
			id: updatedPost.id.value,
			authorEmail: updatedPost.authorEmail.value,
			content: updatedPost.content.value,
			title: updatedPost.title.value,
			comments: updatedPost.comments.map((c) => {
				return {
					id: c.id.value,
					content: c.id.value,
					emailAuthor: c.emailAuthor.value,
					timestamp: c.timestamp.value,
				};
			}),
		};

		return postResponse;
	}
}