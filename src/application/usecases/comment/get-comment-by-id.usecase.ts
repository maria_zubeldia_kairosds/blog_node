import { Service } from 'typedi';
import { ExceptionWithCode } from '../../../domain/model/exception-with-code';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { PostService } from '../../../domain/services/post.service';
import { IdRequest } from '../id.request';
import { CommentResponse } from './comment.response';

@Service()
export class GetCommetByIdUseCase {
	constructor(private postService: PostService){}

	async execute(commentId: IdRequest): Promise<CommentResponse>{
		const commentIdVO = IdVO.createWithUUID(commentId);
		const comment = await this.postService.getCommentById(commentIdVO);

		if (!comment) {
			throw new ExceptionWithCode(400, 'comment not found');
		}

		const commentResponse: CommentResponse = {
			id: comment.id.value,
			emailAuthor: comment.emailAuthor.value,
			content: comment.content.value,
			timestamp: comment.timestamp.value
		};

		console.log('use case ', commentResponse);
		return commentResponse;
	}
}