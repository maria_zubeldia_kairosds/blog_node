export type CommentResponse = {
  id: string;
  emailAuthor: string;
  content: string;
  timestamp: string;
};
