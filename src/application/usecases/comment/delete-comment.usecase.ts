import { Service } from 'typedi';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { PostService } from '../../../domain/services/post.service';

@Service()
export class DeleteCommentUseCase {
	constructor(private postService: PostService) {}

	async execute(request: DeleteCommentRequest): Promise<void> {

		//const idPost = IdVO.createWithUUID(request.idPost);

		const idComment = IdVO.createWithUUID(request.idComment);
		await this.postService.deleteComment(idComment);
	}
}

export type DeleteCommentRequest = {
    //idPost: string,
    idComment: string,
    //user: User
}