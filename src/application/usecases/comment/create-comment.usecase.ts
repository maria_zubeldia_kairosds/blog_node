//request: recibe post y comentario
//response: no devuelve nada

import { Service } from 'typedi';
import {
	Comment,
	CommentType
} from '../../../domain/model/entities/comment.entity';
import { Post } from '../../../domain/model/entities/post.entity';
import { ExceptionWithCode } from '../../../domain/model/exception-with-code';
import { ContentCommentVO } from '../../../domain/model/vos/content-comment.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { TimestampVO } from '../../../domain/model/vos/timestamp.vo';
import { EmailVO } from '../../../domain/model/vos/user/email.vo';
import { PostService } from '../../../domain/services/post.service';
import { CommentRequest } from './comment.request';
import { CommentResponse } from './comment.response';

@Service()
export class AddCommentUseCase {
	constructor(
    private postService: PostService,
		//private userService: UserService
	) {}

	async execute(
		request: CommentRequest,
		userId: string
	): Promise<CommentResponse> {
		const post: Post | null = await this.postService.getById(
			IdVO.createWithUUID(request.postId)
		);

		if (!post) {
			throw new ExceptionWithCode(404, 'post not found');
		}
		/*const author: User | null = await this.userService.getByEmail(
			EmailVO.create(request.userEmail)
		);

		if (!author) {
			throw new ExceptionWithCode(404, 'author not found');
		}*/

		//const idComment = IdVO.create();
		const commentData: CommentType = {
			id: IdVO.create(),
			emailAuthor: EmailVO.create(request.userEmail),
			content: ContentCommentVO.create(request.content),
			timestamp: TimestampVO.create(),
		};

		await this.postService.addComment(post, new Comment(commentData), userId);
		
		const responseData: CommentResponse = {
			id: commentData.id.value,
			emailAuthor: commentData.emailAuthor.value,
			content: commentData.content.value,
			timestamp: commentData.timestamp.value
		};

		//console.log('create comment use case ', commentData.id.value);

		return responseData;
	}
}
