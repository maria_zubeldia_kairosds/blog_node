import 'reflect-metadata';
import Container from 'typedi';
import { Comment } from '../../../domain/model/entities/comment.entity';
import { Post } from '../../../domain/model/entities/post.entity';

import { ContentCommentVO } from '../../../domain/model/vos/content-comment.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { ContentVO } from '../../../domain/model/vos/post/content.vo';
import { TitleVO } from '../../../domain/model/vos/post/title.vo';
import { TimestampVO } from '../../../domain/model/vos/timestamp.vo';
import { EmailVO } from '../../../domain/model/vos/user/email.vo';
import { PostRepositoryPG } from '../../../infraestructure/repositories/post.repository.pg';

import { CommentRequest } from './comment.request';
import { AddCommentUseCase } from './create-comment.usecase';
/*
const user = new User({
	id: IdVO.createWithUUID('133e2a8a-e166-423f-958c-cf5d27604bf1'),
	email: EmailVO.create('maria_kairos@holi.com'),
	password: PasswordVo.create('abcd'),
	role: RoleVO.create(Role.ADMIN),
});*/

jest.mock('../../../infraestructure/repositories/post.repository.pg', () => {
	return {
		PostRepositoryPG: jest.fn().mockImplementation(() => {
			return {
				createComment: jest.fn().mockImplementation(() => {
					return [
						new Post({
							id: IdVO.createWithUUID('43fbc55f-528a-4566-8164-fa4d7d7323da'),
							authorEmail: EmailVO.create('maria_kairosds@kairos.com'),
							title: TitleVO.create('titulo test holiiiiiiiiii'),
							content: ContentVO.create(
								'este es el contenido del post de test asdfffffffffffff'
							),
							comments: [
								new Comment({
									id: IdVO.create(),
									emailAuthor: EmailVO.create('maria_kairosds@kairos.com'),
									content: ContentCommentVO.create(
										'este es el contenido del post de test asdfffffffffffff'
									),
									timestamp: TimestampVO.create(),
								}),
							],
						}),
					];
				}),
				getById: jest.fn(),
			};
		}),
	};
});

describe('create comment use case', () => {
	it('should create a comment', async () => {
		const repo = new PostRepositoryPG();
		//const userRepo = new UserRepositoryPG();
		Container.set('PostRepository', repo);
		//Container.set('UserRepository', userRepo);

		const useCase = Container.get(AddCommentUseCase);
		//const useCaseUser= Container.get()
		const postId = '43fbc55f-528a-4566-8164-fa4d7d7323da';
		const userId = '133e2a8a-e166-423f-958c-cf5d27604bf1';
		
		const req: CommentRequest = {
			postId: '43fbc55f-528a-4566-8164-fa4d7d7323da',
			content: 'este es el contenido del comentario',
			userEmail: 'maria_kairos@holi.com',
		};

		//await useCase.execute(req, userId);

		//expect(repo.addComment).toHaveBeenCalled();
		//expect(userRepo.getById).toHaveBeenCalled();
	});
});
