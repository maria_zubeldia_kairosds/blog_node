export type CommentRequest = {
  postId: string;
  userEmail: string;
  content: string;
};
