import { Service } from 'typedi';
import { User, UserType } from '../../../domain/model/entities/user.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { EmailVO } from '../../../domain/model/vos/user/email.vo';
import { PasswordVo } from '../../../domain/model/vos/user/password.vo';
import { Role, RoleVO } from '../../../domain/model/vos/user/rol.vo';
import { UserService } from '../../../domain/services/user.service';

@Service()
export class SignUpUseCase {

	constructor(private userService: UserService){}
	//genero usuario para llamar al servicio
	async execute(request: SignUpRequest): Promise<void> {

		const user: UserType = {
			id: IdVO.create(),
			email: EmailVO.create(request.email),
			password: PasswordVo.create(request.password),
			role: RoleVO.create(Role[request.role])
		};
		
		await this.userService.persist(new User(user));
	}
}

export type SignUpRequest = {
    email: string;
    password: string;
	role: 'USER' | 'AUTHOR'
	
}