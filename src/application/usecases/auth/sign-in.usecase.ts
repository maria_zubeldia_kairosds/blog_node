import jwt from 'jsonwebtoken';
import { Service } from 'typedi';
import { ExceptionWithCode } from '../../../domain/model/exception-with-code';
import { EmailVO } from '../../../domain/model/vos/user/email.vo';
import { PasswordVo } from '../../../domain/model/vos/user/password.vo';
import { UserService } from '../../../domain/services/user.service';

@Service()
export class SignInUseCase {
	
	constructor(private userService: UserService) {}

	async execute(request: SignInRequest): Promise<string | null> {
		const user = await this.userService.getByEmail(
			EmailVO.create(request.email)
		);

		if (!user) {
			throw new ExceptionWithCode(404, 'User not found');
		}

		const plainPassword = PasswordVo.create(request.password);

		const isValid = await this.userService.isValidPassword(plainPassword, user);

		//autenticación
		if (isValid) {
			//el secreto normalmente es una variable de entorno, es lo que le da seguridad al token
			//user válido: le doy el token
			
			return jwt.sign({ email: user.email.value }, 'secret', {
				expiresIn: 86400, //24h
			});
		}
		return null;
	}
}

export type SignInRequest = {
  email: string;
  password: string;
};
