import bcrypt from 'bcrypt';
import { Inject, Service } from 'typedi';
import { User, UserType } from '../model/entities/user.entity';
import { EmailVO } from '../model/vos/user/email.vo';
import { PasswordVo } from '../model/vos/user/password.vo';
import { UserRepository } from '../repositories/user.repository';

@Service()
export class UserService {
	constructor(
    @Inject('UserRepository') private userRepository: UserRepository
	) {}

	//verificación de password
	async isValidPassword(password: PasswordVo, user: User): Promise<boolean> {
		return bcrypt.compare(password.value, user.password.value);
	}

	async persist(user: User): Promise<void> {
		const hash = await bcrypt.hash(user.password.value, 10);
		const encryptPassword = PasswordVo.create(hash);
		const newUser: UserType = {
			id: user.id,
			email: user.email,
			password: encryptPassword,
			role: user.role
		};
		await this.userRepository.save(new User(newUser));
	}

	async getByEmail(email: EmailVO): Promise<User | null> {
		return this.userRepository.getByEmail(email);
	}

	/*async getRole(user: User): Promise<Role | null> {
		return this.userRepository.getRole(user);
	}*/


}

