//implementación para guardar info de entidad

import { Inject, Service } from 'typedi';
import {
	OffensiveWord,
	OffensiveWordType
} from '../model/entities/offensive-word.entity';
import { IdVO } from '../model/vos/id.vo';
import { LevelVO } from '../model/vos/level.vo';
import { WordVO } from '../model/vos/word.vo';
import { OffensiveWordRepository } from '../repositories/offensive-word.repository';

@Service()
export class OffensiveWordService {
	constructor(
    @Inject('OffensiveWordRepository')
    private offensiveWordReposity: OffensiveWordRepository
	) {}

	async persist(offensiveWord: OffensiveWordType): Promise<void> {
		const offensiveWordEntity = new OffensiveWord(offensiveWord);
		return this.offensiveWordReposity.save(offensiveWordEntity);
	}

	async getAll(): Promise<OffensiveWord[]> {
		return this.offensiveWordReposity.getAll();
	}

	async getById(id: IdVO): Promise<OffensiveWord | null> {
		return this.offensiveWordReposity.getById(id);
	}
	async delete(id: IdVO): Promise<void> {

		await this.offensiveWordReposity.delete(id);
	}

	async update(offensiveWord: OffensiveWord) {
		const originalOffensiveWord = await this.offensiveWordReposity.getById(
			offensiveWord.id
		);

		const newOffensiveWord: OffensiveWordType = {
			id: offensiveWord.id,
			word: WordVO.create(
				offensiveWord.word.value ?? originalOffensiveWord?.word.value
			),
			level: LevelVO.create(
				offensiveWord.level.value ?? originalOffensiveWord?.level.value
			),
		};

		await this.offensiveWordReposity.update(
			new OffensiveWord(newOffensiveWord)
		);
	}
	/*
	private async checkId(id: IdVO): Promise<OffensiveWord> {
		const offensiveWordDB = await this.getById(id);

		if (!offensiveWordDB) {
			throw console.error(404, `id not found ${id.value}`);
		}
		return offensiveWordDB;
	}*/
}
