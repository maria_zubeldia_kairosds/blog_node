import { Inject, Service } from 'typedi';
import { Comment } from '../model/entities/comment.entity';
import { Post, PostType } from '../model/entities/post.entity';
import { IdVO } from '../model/vos/id.vo';
import { ContentVO } from '../model/vos/post/content.vo';
import { TitleVO } from '../model/vos/post/title.vo';
import { EmailVO } from '../model/vos/user/email.vo';
import { PostRepository } from '../repositories/post.repository';

@Service()
export class PostService {
	constructor(
    @Inject('PostRepository')
    private postRepository: PostRepository
	) {}

	async persist(post: Post, userId: string): Promise<void> {
		const postEntity = new Post(post);
		return this.postRepository.save(postEntity, userId);
	}

	async getAll(): Promise<Post[]> {
		return this.postRepository.getAll();
	}

	async getById(id: IdVO): Promise<Post | null> {
		return await this.postRepository.getById(id);
	}

	async delete(id: IdVO): Promise<void> {
		await this.postRepository.delete(id);
	}

	async update(post: Post): Promise<void> {
		const originalPost = await this.postRepository.getById(post.id);

		const newPost: PostType = {
			id: post.id,
			title: TitleVO.create(post.title.value ?? originalPost?.title.value),
			authorEmail: EmailVO.create(
				post.authorEmail.value ?? originalPost?.authorEmail.value
			),
			content: ContentVO.create(
				post.content.value ?? originalPost?.content.value
			),
			comments: post.comments,
		};

		await this.postRepository.update(new Post(newPost));
	}

	async addComment(post: Post, comment: Comment, userId: string): Promise<void> {
		post.addComments(comment);
		await this.postRepository.addComment(post, new Comment(comment), userId);
	}

	async getCommentById(commentId: IdVO): Promise<Comment | null>{
		const comment: Comment | null = await this.postRepository.getCommentById(
			commentId
		);
		
		if (!comment) {
			throw new Error('comment not found');
		}

		console.log('comment servicio ', comment);
		
		return comment;
	}
	
	async deleteComment(commentId: IdVO): Promise<void> {
		//const post: Post | any = await this.getById(postId);
		await this.postRepository.deleteComment(commentId);
	}
}
