import { LevelVO } from './level.vo';

describe('Level VO', () => {

	it ('should create', () => {
		const minLevel = 1;
		const level = LevelVO.create(minLevel);
		expect(level.value).toEqual(minLevel);
	});

	it('should throw error if level is highier than 5', () => {
		const invalidLevel = 6;
		//const level = LevelVO.create(invalidLevel);
		expect(() => LevelVO.create(invalidLevel)).toThrow(
			`${invalidLevel} no es válido. Tiene que estar entre 1 y 5`
		);
	});

	it('should throw error if level is lower than 1', () => {
		const invalidLevel = 0;
		//const level = LevelVO.create(invalidLevel);
		expect(() => LevelVO.create(invalidLevel)).toThrow(
			`${invalidLevel} no es válido. Tiene que estar entre 1 y 5`
		);
	});

});