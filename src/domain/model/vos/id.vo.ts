import { v4, validate } from 'uuid';
import { ExceptionWithCode } from '../exception-with-code';

export class IdVO {
	get value(): string {
		return this.id;
	}

	private constructor(private id: string) {}

	static createWithUUID(uuid: string): IdVO {
		if (!validate(uuid)) {
			throw new ExceptionWithCode(400, `${uuid} node es un UUID`);
		}
		return new IdVO(uuid);
	}

	static create(): IdVO {
		return new IdVO(v4());
	}
}