import { WriterNameVO } from './writer-name.vo';
describe('Writer name VO', () => {
	it ('should create', () => {
		const newWriterName = 'Jose Manuel';
		const writerName = WriterNameVO.create(newWriterName);
		expect(writerName.value).toEqual(newWriterName);
	});

	it('should throw error if writer name is shorter than 5', () => {
		const shortName = 'Pepa';
		expect(() => WriterNameVO.create(shortName)).toThrow(
			`${shortName} tiene que contener entre 5 y 30 caracteres.`
		);
	});

	it('should throw error if writer name is longer than 30', () => {
		const longName = 'Este nombre es muy largo para estar aqui';
		expect(() => WriterNameVO.create(longName)).toThrow(
			`${longName} tiene que contener entre 5 y 30 caracteres.`
		);
	});

	it('should throw error if writer name is empty', () => {
		const emptyName = '';
		expect(() => WriterNameVO.create(emptyName)).toThrow(
			'El nombre del autor es obligatorio.'
		);
	});
});