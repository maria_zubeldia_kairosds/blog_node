import { WriterNickNameVO } from './writer-nickname.vo';

describe('Writer nickname VO', () => {
	it('should create', () => {
		const newWriterNickname = 'Silas';
		const writerNickname = WriterNickNameVO.create(newWriterNickname);
		expect(writerNickname.value).toEqual(newWriterNickname);
	});

	it('should throw error if nickname is shorter than 3', () => {
		const shortNickName = 'Si';
		expect(() => WriterNickNameVO.create(shortNickName)).toThrow(
			`${shortNickName} tiene que contener entre 3 y 10 caracteres.`
		);
	});

	it('should throw error if nickname is longer than 10', () => {
		const longNickname = 'Este nombre es muy largo para estar aqui';
		expect(() => WriterNickNameVO.create(longNickname)).toThrow(
			`${longNickname} tiene que contener entre 3 y 10 caracteres.`
		);
	});

	it('should throw error if nickname is empty', () => {
		const emptyName = '';
		expect(() => WriterNickNameVO.create(emptyName)).toThrow(
			'El nickname del autor es obligatorio.'
		);
	});
});