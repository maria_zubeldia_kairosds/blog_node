import { ExceptionWithCode } from '../../exception-with-code';

export class ContentVO {
	get value(): string{
		return this.text;
	}

	private constructor(private text: string){}

	static create (text: string): ContentVO {
		if (!text) {
			throw new ExceptionWithCode(400, 'Inserte un texto.');
		} else if (text.length < 50 || text.length > 300) {
			throw new ExceptionWithCode(400,
				`${text} tiene que contener entre 50 y 300 caracteres.`
			);
		}
		return new ContentVO(text);
	}
}