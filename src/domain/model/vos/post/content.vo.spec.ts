import { ContentVO } from './content.vo';

describe('Text VO', () => {
	it('should create', () => {
		const newText =
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ';
		const createText = ContentVO.create(newText);
		expect(createText.value).toEqual(newText);
	});

	it('should throw error if text is shorter than 50', () => {
		const shortTitle = 'Pepa';
		expect(() => ContentVO.create(shortTitle)).toThrow(
			`${shortTitle} tiene que contener entre 50 y 300 caracteres.`
		);
	});

	it('should throw error if text is longer than 300', () => {
		const longTitle =
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.';
		expect(() => ContentVO.create(longTitle)).toThrow(
			`${longTitle} tiene que contener entre 50 y 300 caracteres.`
		);
	});

	it('should throw error if text is empty', () => {
		const emptyText = '';
		expect(() => ContentVO.create(emptyText)).toThrow('Inserte un texto.');
	});
});
