import { ExceptionWithCode } from '../../exception-with-code';

export class WriterNameVO {
	get value(): string {
		return this.writerName;
	}

	private constructor(private writerName: string){}

	static create(writerName: string): WriterNameVO {
		if (!writerName) {
			throw new ExceptionWithCode(400,'El nombre del autor es obligatorio.');
		} else if  (writerName.length < 5 || writerName.length > 30) {
			throw new ExceptionWithCode(400,`${writerName} tiene que contener entre 5 y 30 caracteres.`);
		}
		return new WriterNameVO(writerName);
	}
}