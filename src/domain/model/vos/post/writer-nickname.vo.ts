import { ExceptionWithCode } from '../../exception-with-code';

export class WriterNickNameVO {
	get value(): string {
		return this.writerNickname;
	}

	private constructor(private writerNickname: string) {}

	static create(writerNickname: string): WriterNickNameVO {
		if (!writerNickname) {
			throw new ExceptionWithCode(400, 'El nickname del autor es obligatorio.');
		} else if (writerNickname.length < 3 || writerNickname.length > 10) {
			throw new ExceptionWithCode(400,
				`${writerNickname} tiene que contener entre 3 y 10 caracteres.`
			);
		}
		return new WriterNickNameVO(writerNickname);
	}
}