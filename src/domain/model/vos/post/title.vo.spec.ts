import { TitleVO } from './title.vo';
describe('Title VO', () => {
	it('should create', () => {
		const newTitle = 'Esto es un título dummy';
		const createTitle = TitleVO.create(newTitle);
		expect(createTitle.value).toEqual(newTitle);
	});

	it('should throw error if title is shorter than 5', () => {
		const shortTitle = 'Pepa';
		expect(() => TitleVO.create(shortTitle)).toThrow(
			`${shortTitle} tiene que contener entre 5 y 30 caracteres.`
		);
	});

	it('should throw error if title is longer than 30', () => {
		const longTitle = 'Este nombre es muy largo para estar aqui';
		expect(() => TitleVO.create(longTitle)).toThrow(
			`${longTitle} tiene que contener entre 5 y 30 caracteres.`
		);
	});

	it('should throw error if title is empty', () => {
		const emptyTitle = '';
		expect(() => TitleVO.create(emptyTitle)).toThrow(
			'El título del post es obligatorio.'
		);
	});
});
