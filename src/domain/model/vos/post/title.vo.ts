import { ExceptionWithCode } from '../../exception-with-code';

export class TitleVO {
	get value():string {
		return this.title;
	}

	private constructor(private title: string) {}

	static create (title: string) : TitleVO{
		if (!title) {
			throw new ExceptionWithCode(400, 'El título del post es obligatorio.');
		} else if (title.length < 5 || title.length > 30) {
			throw new ExceptionWithCode(400, `${title} tiene que contener entre 5 y 30 caracteres.`);
		}
		return new TitleVO(title);
	}
}