export class PasswordVo {
    
	get value(): string{
		return this.password;
	}

	private constructor(private password: string){}

	static create(password: string): PasswordVo{
		return new PasswordVo(password);
	}
}