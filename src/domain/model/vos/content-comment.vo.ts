import { ExceptionWithCode } from '../exception-with-code';

export class ContentCommentVO {

	private static readonly MIN_LENGTH: 2;
	private static readonly MAX_LENGTH = 200;

	get value(): string {
		return this.contentComment;
	}

	private constructor(private contentComment: string) {}

	static create(contentComment: string): ContentCommentVO {

		if (contentComment.length < this.MIN_LENGTH) {
			throw new ExceptionWithCode(400, 'Comentario demasiado corto, mínimo 2 caracteres');
		}
		if (contentComment.length > this.MAX_LENGTH){
			throw new ExceptionWithCode(400, 'Comentario demasiado largo, máximo 200 caracteres');
		}

		return new ContentCommentVO(contentComment);
	}
}