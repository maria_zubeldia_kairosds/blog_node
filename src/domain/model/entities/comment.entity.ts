import { ContentCommentVO } from '../vos/content-comment.vo';
import { IdVO } from '../vos/id.vo';
import { TimestampVO } from '../vos/timestamp.vo';
import { EmailVO } from '../vos/user/email.vo';

export type CommentType = {
    id: IdVO,
    emailAuthor: EmailVO,
    content: ContentCommentVO,
    timestamp: TimestampVO
}

export class Comment {
	constructor(private comment: CommentType) {}

	get id(): IdVO {
		return this.comment.id;
	}

	get emailAuthor(): EmailVO {
		return this.comment.emailAuthor;
	}

	get content(): ContentCommentVO {
		return this.comment.content;
	}

	get timestamp(): TimestampVO {
		return this.comment.timestamp;
	}
}