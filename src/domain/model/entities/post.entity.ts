import { IdVO } from '../vos/id.vo';
import { ContentVO } from '../vos/post/content.vo';
import { TitleVO } from '../vos/post/title.vo';
import { EmailVO } from '../vos/user/email.vo';
import { Comment } from './comment.entity';

export type PostType = {
  id: IdVO;
  authorEmail: EmailVO;
  title: TitleVO;
  content: ContentVO;
  comments: Comment[];
};

export class Post {
	constructor(private post: PostType) {}

	get id(): IdVO {
		return this.post.id;
	}
	get authorEmail(): EmailVO {
		return this.post.authorEmail;
	}

	get title(): TitleVO {
		return this.post.title;
	}

	get content(): ContentVO {
		return this.post.content;
	}

	get comments(): Comment[] {
		//console.log('metodo get comments entidad post ', this.post.comments);
		
		return this.post.comments;
	}

	addComments(comment: Comment): void {
		console.log('entidad post comment ', comment);
		console.log('entidad post this.post.comments ', this.post.comments);
		let postComments: Comment[] = this.post.comments;
		console.log('let postcomments ', postComments);
		
		postComments = [...postComments, comment];
	}

	getComment(idComment: IdVO): Comment | undefined {
		return this.post.comments.find((c) => c.id.value === idComment.value);
	}

	deleteComment(idComment: IdVO): void {
		this.post.comments = this.post.comments.filter(
			(c) => c.id.value !== idComment.value
		);
	}
}
