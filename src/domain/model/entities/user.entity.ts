import { IdVO } from '../vos/id.vo';
import { EmailVO } from '../vos/user/email.vo';
import { PasswordVo } from '../vos/user/password.vo';
import { RoleVO } from '../vos/user/rol.vo';

export type UserType = {
  id: IdVO;
  email: EmailVO;
  password: PasswordVo;
  role: RoleVO
};

export class User {
	constructor(private user: UserType) {}

	get id(): IdVO {
		return this.user.id;
	}

	get email(): EmailVO {
		return this.user.email;
	}

	get password(): PasswordVo {
		return this.user.password;
	}

	get role(): RoleVO{
		return this.user.role;
	}
}
