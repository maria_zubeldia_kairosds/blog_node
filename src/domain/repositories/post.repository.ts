import { Comment } from '../model/entities/comment.entity';
import { Post } from '../model/entities/post.entity';
import { IdVO } from '../model/vos/id.vo';

export interface PostRepository {
  save(post: Post, userId: string): Promise<void>;

  getAll(): Promise<Post[]>;

  getById(id: IdVO): Promise<Post | null>;

  delete(id: IdVO): Promise<void>;

  update(post: Post): Promise<void>;

  addComment(post: Post, comment: Comment, userId: string): Promise<void>;

  getCommentById(commentId: IdVO): Promise<Comment | null>;

  deleteComment(commentId: IdVO): Promise<void>;
}