import { User } from '../model/entities/user.entity';
import { EmailVO } from '../model/vos/user/email.vo';

export interface UserRepository {
    save(user: User): Promise<void>;

    getByEmail(email: EmailVO): Promise<User | null>;

    //getRole(user:User): Promise<Role | null>;
    
}