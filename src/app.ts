import 'reflect-metadata';

import { config } from 'dotenv';
config();

import { json } from 'body-parser';
import cors from 'cors';
import express from 'express';
import expressPinoLogger from 'express-pino-logger';
import passport from 'passport';
import Container from 'typedi';
import { logger } from './infraestructure/config/logger';
import { connectToDB } from './infraestructure/config/mongo';
import './infraestructure/config/postgresql';
import passportMiddleWare from './infraestructure/middlewares/passport';
import { CommentModel } from './infraestructure/repositories/comment.schema';
import { OffensiveWordRepositoryMongo } from './infraestructure/repositories/offensive-word.repository.mongo';
import { PostRepositoryPG } from './infraestructure/repositories/post.repository.pg';
import { PostModel } from './infraestructure/repositories/post.schema';
import { UserRepositoryPG } from './infraestructure/repositories/user.repository.pg';
import { UserModel } from './infraestructure/repositories/users.schema';
import { authRouter } from './infraestructure/routes/auth.route';
import { offensiveWordRouter } from './infraestructure/routes/offensive-word.route';
import { postsRouter } from './infraestructure/routes/posts.routes';

Container.set('OffensiveWordRepository', new OffensiveWordRepositoryMongo());

Container.set('UserRepository', new UserRepositoryPG());

Container.set('PostRepository', new PostRepositoryPG());

connectToDB();

console.log('App started');
console.log('probando logs');

const app = express();
app.use(cors());
app.use(json());
app.use(expressPinoLogger(logger));
app.use(offensiveWordRouter);
app.use(postsRouter);
app.use(authRouter);
app.use(passport.initialize());
passport.use(passportMiddleWare);

app.listen(3000, () => {
	console.log('server started');
	UserModel.hasMany(PostModel);
	UserModel.hasMany(CommentModel);
	PostModel.belongsTo(UserModel);
	PostModel.hasMany(CommentModel);
	CommentModel.belongsTo(PostModel);
	CommentModel.belongsTo(UserModel);
});
